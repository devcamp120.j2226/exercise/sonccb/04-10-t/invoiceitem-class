public class InvoiceItem {
 /**
  * Các thuộc tính
  */
 private String id;
 private String desc;
 private int qty;
 private double unitPrice;
 /**
  * Phương thức khởi tạo
  */
 public InvoiceItem(String id,String desc, int qty, double unitPrice) {
  this.id = id;
  this.desc = desc;
  this.qty = qty;
  this.unitPrice = unitPrice;
 }
 /**
 * Getter method
 * @return
 */
 public String getId(){
  return this.id;
 }

 public String getDesc(){
  return this.desc;
 }

 public int getQty(){
  return this.qty;
 }
 public double getUnitPrice(){
  return this.unitPrice;
 }

 public double getTotal(){
  return this.unitPrice * this.qty;
 }
 /**
 * Setter method
 * @return
 */
 public void setQty(int qty){
  this.qty = qty;
 }

 public void setUnitPrice(double unitPrice){
  this.unitPrice = unitPrice;
}
/**
 *  Ỉn ra cosolelog
 */
 public String toString(){
  return "InvoiceItem[id="+ id + ", desc=" + desc +", qty-"+ qty + ", unitPrice=" + unitPrice + "]";
 }
}